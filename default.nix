# This is a minimal `default.nix` by yarn-plugin-nixify. You can customize it
# as needed, it will not be overwritten by the plugin.

{ pkgs ? import <nixpkgs> { } }:

(pkgs.callPackage ./yarn-project.nix { } { src = ./.; }).overrideAttrs (prev: {
  name = "mx-puppet-slack";
  buildInputs = with pkgs; prev.buildInputs ++ [ python3 pkg-config pixman cairo pango ];
  preConfigure = ''
    patchShebangs .
  '';
  buildPhase = ''
    runHook preBuild
    yarn build
    runHook postBuild
  '';
  # postInstall = ''
  #   cat <<EOF > $out/bin/mx-puppet-slack
  #   #!/bin/sh
  #   exec ${pkgs.nodejs}/bin/node $out/libexec/mx-puppet-slack/build/index.js "\$@"
  #   EOF
  #   chmod +x $out/bin/mx-puppet-slack
  # '';
  preInstall = with pkgs; ''
    sed -i /^build$/d .gitignore
    ${jq}/bin/jq '. + {"bin":"build/index.js"}' package.json | ${moreutils}/bin/sponge package.json
  '';
})
